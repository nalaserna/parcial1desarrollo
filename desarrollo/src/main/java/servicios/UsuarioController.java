package servicios;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Usuario;
import repositories.UsuarioRepository;

@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	
	@RequestMapping("/getAllUsers")
	public Iterable<Usuario> getAllUsers () {
		
		Iterable<Usuario> findAll = usuarioRepositoryDAO.findAll();
		
		return findAll;
		
	}
	
	@RequestMapping(path="/addCliente", method=RequestMethod.POST) 
	public @ResponseBody String addNewCliente 
	(@RequestParam String nombre, @RequestParam String apellido) {
		
		Usuario nuevoUsuario = new Usuario();
		nuevoUsuario.setNombre(nombre);
		nuevoUsuario.setApellido(apellido);
		nuevoUsuario.setTipoUsuario("Cliente");
		usuarioRepositoryDAO.save(nuevoUsuario);
		return "Cliente Guardado";
		
	}
	
	@RequestMapping(path="/addConsultor", method=RequestMethod.POST) 
	public @ResponseBody String addConsultor 
	(@RequestParam String nombre, @RequestParam String apellido) {
		
		Usuario nuevoUsuario = new Usuario();
		nuevoUsuario.setNombre(nombre);
		nuevoUsuario.setApellido(apellido);
		nuevoUsuario.setTipoUsuario("Consultor");
		usuarioRepositoryDAO.save(nuevoUsuario);
		return "Consultor Guardado";
		
	}
	@RequestMapping ("/getUserById")
	public Usuario getUserById (@RequestParam int id) {
		
		return usuarioRepositoryDAO.findById(id);
		
	}
}
