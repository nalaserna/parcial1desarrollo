package servicios;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Servicio;
import entities.Usuario;
import repositories.ServicioRepository;
import repositories.UsuarioRepository;

@RestController
public class ServicioController {
	
	@Autowired
	private ServicioRepository servicioRepositoryDAO;
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	
	@RequestMapping("/getAllServicios")
	public Iterable<Servicio> getAllServicios () {
		
		Iterable<Servicio> findAll = servicioRepositoryDAO.findAll();
		
		return findAll;
		
	}
	
	@RequestMapping(path="/addServicio", method=RequestMethod.POST) 
	public @ResponseBody String addNewServicio 
	(@RequestParam String categoria, @RequestParam String descripcion, @RequestParam String imagen, @RequestParam int consultor_id) {
		
		Usuario consultor = new Usuario();
		consultor = usuarioRepositoryDAO.findById(consultor_id);
				
		Servicio nuevoServicio = new Servicio();
		nuevoServicio.setCategoria(categoria);
		nuevoServicio.setDescripcion(descripcion);
		nuevoServicio.setImagen(imagen);
		nuevoServicio.setConsultor(consultor);
		servicioRepositoryDAO.save(nuevoServicio);
		return "Servicio Guardado";
		
	}
	
	@RequestMapping ("/getServicioById")
	public Servicio getServicioById (@RequestParam int id) {
		
		return servicioRepositoryDAO.findById(id);
		
	}
	
	@RequestMapping(path="/consultaServiciosConsultor", method=RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Servicio> consultaServiciosConsultor 
	(@RequestParam int consultor_id) {
		
		Usuario consultor = new Usuario();
		consultor = usuarioRepositoryDAO.findById(consultor_id);
		List<Servicio> servicios = servicioRepositoryDAO.findByConsultor(consultor);
		
		return servicios;
	}
}
