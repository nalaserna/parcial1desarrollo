package servicios;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import entities.Cotizacion;
import entities.Servicio;
import entities.Usuario;
import repositories.CotizacionRepository;
import repositories.ServicioRepository;
import repositories.UsuarioRepository;

@RestController
public class CotizacionController {
	
	@Autowired
	private CotizacionRepository cotizacionRepositoryDAO;
	@Autowired
	private UsuarioRepository usuarioRepositoryDAO;
	@Autowired
	private ServicioRepository servicioRepositoryDAO;
	
	@RequestMapping("/getAllCotizacion")
	public Iterable<Cotizacion> getAllUsers () {
		
		Iterable<Cotizacion> findAll = cotizacionRepositoryDAO.findAll();
		
		return findAll;
		
	}
	
	@RequestMapping(path="/addCotizacion", method=RequestMethod.POST) 
	public @ResponseBody String addNewCotizacion 
	(@RequestParam String descripcion, @RequestParam int cliente_id,  @RequestParam int servicio_id) {
		
		Usuario cliente = new Usuario();
		cliente = usuarioRepositoryDAO.findById(cliente_id);
		
		Servicio servicio = new Servicio();
		servicio = servicioRepositoryDAO.findById(servicio_id);
		
		Cotizacion nuevaCotizacion = new Cotizacion();
		nuevaCotizacion.setDescripcion(descripcion);
		nuevaCotizacion.setCliente(cliente);
		nuevaCotizacion.setServicio(servicio);
		cotizacionRepositoryDAO.save(nuevaCotizacion);
		return "Cotización Guardada";
		
	}
	
	@RequestMapping(path="/consultaCotizacionConsultor", method=RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Cotizacion> consultaCotizacionConsultor 
	(@RequestParam int consultor_id) {
		
		Usuario consultor = new Usuario();
		consultor = usuarioRepositoryDAO.findById(consultor_id);
		
		//List<Servicio> servicios = servicioRepositoryDAO.findByConsultor(consultor);
		List<Cotizacion> cot = cotizacionRepositoryDAO.findByServicio_consultor(consultor);
		
		/*for(int i=0; i<servicios.size(); i++ ) {
			cotizaciones = cotizacionRepositoryDAO.findByServicio(servicios.get(i));
			System.out.println(cotizaciones);
		}*/
		return cot;
	}
	
	@RequestMapping(path="/consultaCotizacionServicio", method=RequestMethod.GET, produces = "application/json") 
	public @ResponseBody List<Cotizacion> consultaCotizacionServicio
	(@RequestParam int servicio_id) {
		
		Servicio servicio = servicioRepositoryDAO.findById(servicio_id);
		List<Cotizacion> cotizaciones = cotizacionRepositoryDAO.findByServicio(servicio);
		return cotizaciones;
		
	}
	

	

}
