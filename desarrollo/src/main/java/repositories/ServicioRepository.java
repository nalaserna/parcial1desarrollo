package repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import entities.Cotizacion;
import entities.Servicio;
import entities.Usuario;

@Repository
public interface ServicioRepository extends CrudRepository<Servicio, Long> {
	
	public Servicio findById(int id);
	
	public List<Servicio> findByCategoria(String Categoria);
	
	public List<Servicio> findByConsultor(Usuario consultor);
	
	
}
