package repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import entities.Cotizacion;
import entities.Servicio;
import entities.Usuario;

public interface CotizacionRepository extends CrudRepository<Cotizacion, Long> {
	
	public Cotizacion findById(int id);
	
	public List<Cotizacion> findByServicio(Servicio servicio);
	
	public List<Cotizacion> findByServicio_consultor(Usuario consultor);

}
